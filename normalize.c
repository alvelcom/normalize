/*
 * Implmentations of normalize functions
 */
#include "normalize.h"

#include <stdlib.h>
#include <string.h>

#define MAX_PATH_COMPONENTS 1000

char *normalize_naive(const char *path) {
    char *out, *pos;
    pos = out = malloc(strlen(path) + 3);
    if (!out)
        return NULL;

    *pos++ = '/';

    while (path[0]) {
        char *next = strchr(path, '/');
        int isLast = next == NULL;
        if (isLast)
            next = strchr(path, 0);

        if (next - path == 0 && path[0] == '/') {
            // Ignore double slashes: // -> /
        }
        else if (next - path == 1 && path[0] == '.') {
            // Ignore dot: /. -> /
        } else if (next - path == 2 && path[0] == '.' && path[1] == '.') {
            // Move up: /abc/.. -> /
            if (pos > out + 1)
                pos--;
            while (pos > out + 1 && pos[-1] != '/')
                pos--;
        } else {
            // Copy: /abc -> /abc
            size_t len = next - path + 1;
            strncpy(pos, path, len);
            pos += len;
        }
        path = next + (isLast ? 0 : 1);
        if (pos > out + 1)
            pos[-1] = *next;
    }

    pos[0] = 0;

    return out;
}


char *normalize_split(const char *path) {
    const char *components[MAX_PATH_COMPONENTS];
    int  sizes[MAX_PATH_COMPONENTS];

    int pos = 0, size = 1;
    while (path[0]) {
        char *next = strchr(path, '/');
        int isLast = next == NULL;
        if (isLast)
            next = strchr(path, 0);

        if (next - path == 0) {
        } else if (next - path == 1 && path[0] == '.') {
        } else if (next - path == 2 && path[0] == '.' && path[1] == '.') {
            if (pos > 0) {
                pos--;
                size -= 1 + sizes[pos];
            }
        } else {
            components[pos] = path;
            sizes[pos++] = next - path;
            size += 1 + next - path;
        }
        path = next + (isLast ? 0 : 1);
        if ((!isLast && !path[0]) || (isLast && pos == 0)) {
            components[pos] = "";
            sizes[pos++] = 0;
            size += 1;
        }
        if (pos == MAX_PATH_COMPONENTS)
            return NULL;
    }

    char *out, *it;
    it = out = malloc(size);
    if (!out)
        return NULL;
    for (int i = 0; i < pos; i++) {
        *it++ = '/';
        strncpy(it, components[i], sizes[i]);
        it += sizes[i];
    }
    *it = 0;
    return out;
}

