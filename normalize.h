#ifndef _NORMALIZE_H_
#define _NORMALIZE_H_

#define normalize normalize_split

char *normalize_naive(const char *path);
char *normalize_split(const char *path);

#endif

