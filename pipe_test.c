#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>

#include "normalize.h"

typedef char *normalize_f(const char *path);

#define BUF_SIZE (1024*1024)
static char buffer[BUF_SIZE];

void sighandler(int signum)
{
    fprintf(stderr, "Bad string is '%s'\n", buffer);
    exit(1);
}


int main(int argc, char **argv) {
    normalize_f *func;

    if (argc != 2) {
        puts("Expected 1 argument, please choose: naive or split");
        exit(1);
    }

    if (0 == strcmp(argv[1], "naive"))
        func = normalize_naive;
    else
        func = normalize_split;

    signal(SIGSEGV, sighandler);

    while (1) {
        fgets(buffer, BUF_SIZE, stdin);
        char *end = strchr(buffer, '\n');
        if (!end)
            return 0;
        *end = 0;
        char *value = func(buffer);
        puts(value);
        free(value);
    }
    return 0;
}

