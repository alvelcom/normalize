# normalize
Repo contains two different solutions for the `normalize` function alongside with
tests and benchmarks.

## Build
Ensure cc, cmake, make and git are installed. I've tested it on ubuntu, because I've problems
with building hdr_histogram on freebsd.

    cmake . && make

After build in the the root project directory we should have three binaries:

- `Test` is hand written tests and benchmarks.
- `Fuzz` is generator of random test pairs.
- `PipeTest` is tool, which normalizes every line from stdin and then prints it to stdout.

## Example session

	$ ./Test
	Tests for normalize_naive
	Tests for normalize_split
	bench_empty >
		   0.06       0.06       0.07       0.07       0.08       0.08      13.51 [μs = 10⁻⁶ s]
			0th       10th       30th       50th       70th       90th      100th [pp]
	bench_naive_big >
		   7.72       7.78       8.47       8.51       8.82      12.33      47.28 [μs = 10⁻⁶ s]
			0th       10th       30th       50th       70th       90th      100th [pp]
	bench_split_big >
		   5.67       5.69       5.70       5.71       5.71       6.39      18.83 [μs = 10⁻⁶ s]
			0th       10th       30th       50th       70th       90th      100th [pp]
    $ # Manual tests are passed for both functions, good
    $ # bench_empty is just proof of concept measurement, basically used for estimation of accuracy.
    $ # This benchmark is intended for comparison of functions, so split version is a clear winner.
    $
    $ ./Fuzz 123 1000000 100 40  > input.txt 2> pattern.txt 
    $ # 123 is initial seed
    $ # Generate 1000000 testcases, each up is to 100 path components, each is up to 40 symbols
    $
    $  perf stat ./PipeTest naive < input.txt > output.txt

	 Performance counter stats for './PipeTest naive':

		   2163,519968      task-clock (msec)         #    1,000 CPUs utilized
					 4      context-switches          #    0,002 K/sec
					 0      cpu-migrations            #    0,000 K/sec
					49      page-faults               #    0,023 K/sec
		 7 285 419 522      cycles                    #    3,367 GHz
		 2 696 874 094      stalled-cycles-frontend   #   37,02% frontend cycles idle
		 2 033 679 231      stalled-cycles-backend    #   27,91% backend  cycles idle
		 8 132 793 879      instructions              #    1,12  insns per cycle
													  #    0,33  stalled cycles per insn
		 1 874 902 501      branches                  #  866,598 M/sec
		   145 464 462      branch-misses             #    7,76% of all branches

		   2,164307035 seconds time elapsed

	$ # use normalize_naive function for this test
	$ # this is sort of $file_size/$time = ~260 MBytes/sec/core
    $ diff -u output.txt pattern.txt
    $ # empty, good
	$ perf stat ./PipeTest split < input.txt > output.txt

	 Performance counter stats for './PipeTest split':

		   1848,095591      task-clock (msec)         #    1,000 CPUs utilized
					 4      context-switches          #    0,002 K/sec
					 0      cpu-migrations            #    0,000 K/sec
					52      page-faults               #    0,028 K/sec
		 6 167 610 289      cycles                    #    3,337 GHz
		 2 388 943 260      stalled-cycles-frontend   #   38,73% frontend cycles idle
		 1 756 815 587      stalled-cycles-backend    #   28,48% backend  cycles idle
		 6 774 299 150      instructions              #    1,10  insns per cycle
													  #    0,35  stalled cycles per insn
		 1 442 215 850      branches                  #  780,379 M/sec
		   108 070 293      branch-misses             #    7,49% of all branches

		   1,848516852 seconds time elapsed

	$ # this is sort of ~305 MBytes/sec/core
    $ diff -u output.txt pattern.txt
    $ # empty, excellent
	$ lscpu | grep -E '(Model|MHz)'
	Model:                 42
	Model name:            Intel(R) Core(TM) i7-2640M CPU @ 2.80GHz
	CPU MHz:               801.062
	CPU max MHz:           3500.0000
	CPU min MHz:           800.0000
	$ free -h
				  total        used        free      shared  buff/cache   available
	Mem:           7.6G        1.2G        2.0G        153M        4.4G        5.9G

## Analysis

Not sure that this is a good idea to use MBytes/sec/core because of:

- this depends on hardware;
- this depends on a way benchmark is done;
- this depends on test data.

But, I guess, it's okay for A/B testing, where we can controll all of this.


I'm not really expert in CPU caches, but it seems like both algorithms use mostly sequential
pattern of access, so that CPU should be able to recognize it and prefetch next part `path`.
This makes sense only for long lines (order of size L2), because small lines would most
likely be in L2 cache anyway, and therefore we don't have to wait for ages for main RAM.

Here is more detail description of algorithms:

- `naive`: First pass is strlen, second is chrchr, trird is strcpy, and sort of fourth
  is backtracking, consider example `/really-long-line/..`. Here we will traverse the
  biggest component 4 times. In most cases a component of path should be able to fit into
  L1 cache, so that latter 3 times accesses are cheap.
- `split`: First pass is gathering information and sequentially puts it into array.
  Second and final pass builds an resulting string.

