#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include <hdr/hdr_histogram.h>
#include <hdr/hdr_time.h>

#include "normalize.h"

static int64_t diff(struct timespec t0, struct timespec t1)
{
    int64_t delta_us = 0;
    delta_us = (t1.tv_sec - t0.tv_sec) * 100000000;
    delta_us += (t1.tv_nsec - t0.tv_nsec) / 10;

    return delta_us;
}

#define BENCH(func) bench(#func, func)
typedef void bench_f();
void bench(const char *name, bench_f function) {
    struct timespec t0, t1;
    struct hdr_histogram* histogram;

    hdr_init(/* minimal value = 0.01μs */ 1,
             /* maximal value = 10ms */ 10 * 1000 * 100,
             /* #significant digits */ 4,
             &histogram);

    printf("%s >\n", name);
    function();

    for (int i = 0; i < 100000; i++) {
        clock_gettime(CLOCK_MONOTONIC, &t0);
        function();
        clock_gettime(CLOCK_MONOTONIC, &t1);
        hdr_record_value(histogram, diff(t0, t1));
    }

    int percentiles[] = {0, 10, 30, 50, 70, 90, 100};
    for (int i = 0; i < sizeof(percentiles) / sizeof(*percentiles); i++)
        printf("%11.2lf", hdr_value_at_percentile(histogram, percentiles[i]) * 0.01);
    puts(" [μs = 10⁻⁶ s]");
    for (int i = 0; i < sizeof(percentiles) / sizeof(*percentiles); i++)
        printf("%9dth", percentiles[i]);
    puts(" [pp]");
    free(histogram);
}

void bench_empty() { }

typedef char *test_f(const char *path);
void test(test_f func, const char *path, const char *expected) {
    char *result = func(path);
    if (0 != strcmp(result, expected))
        printf("mismatch: path='%s' expected='%s' got='%s'\n", path, expected, result);
    free(result);
}

#define TESTS(func) tests(#func, func)
void tests(const char *name, test_f func) {
    printf("Tests for %s\n", name);

    test(func, "/", "/");
    test(func, "/abc", "/abc");
    test(func, "/abc/",  "/abc/");
    test(func, "/abc/def",  "/abc/def");
    test(func, "/abc/def/",  "/abc/def/");
    test(func, "../abc/def/",  "/abc/def/");

    test(func, "/abc/def/ghi",  "/abc/def/ghi");
    test(func, "/abc/def/ghi/",  "/abc/def/ghi/");
    test(func, "/abc/def/ghi/jkl",  "/abc/def/ghi/jkl");
    test(func, "/abc/def/ghi/jkl/",  "/abc/def/ghi/jkl/");

    test(func, "/.",  "/");
    test(func, "../.",  "/");
    test(func, "/abc/.",  "/abc");
    test(func, "/abc/./",  "/abc/");
    test(func, "/abc/./def",  "/abc/def");
    test(func, "/abc/./def/./ghi",  "/abc/def/ghi");
    test(func, "/abc/def/ghi/.",  "/abc/def/ghi");
    test(func, "/abc/def/ghi/jkl/./",  "/abc/def/ghi/jkl/");

    test(func, "./..",  "/");
    test(func, "/abc/..",  "/");
    test(func, "/abc/../",  "/");
    test(func, "/abc/../def",  "/def");
    test(func, "/abc/../def/../ghi",  "/ghi");
    test(func, "/abc/def/ghi/..",  "/abc/def");
    test(func, "/abc/def/ghi/jkl/../",  "/abc/def/ghi/");
    test(func, "/abc/def/ghi/jkl/..",  "/abc/def/ghi");
    test(func, "/abc/./def/ghi/..",  "/abc/def");

    test(func, "/../.",  "/");
    test(func, "/.././",  "/");
    test(func, "/./..",  "/");
    test(func, "/./../",  "/");
    test(func, "//a/",  "/a/");
    test(func, "..//a/",  "/a/");

}

char big_path[10000];

void bench_naive_big() {
    normalize_naive(big_path);
}

void bench_split_big() {
    normalize_split(big_path);
}

int main() {
    TESTS(normalize_naive);
    TESTS(normalize_split);

    char *ptr = big_path;
    for (int i = 0; i < 100; i++) {
        char *s = "/abc//../ddddddddddddddd/../123/";
        strcpy(ptr, s);
        ptr += strlen(s);
    }

    BENCH(bench_empty);
    BENCH(bench_naive_big);
    BENCH(bench_split_big);
    /**/
    return 0;
}
