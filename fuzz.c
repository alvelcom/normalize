#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_SIZE 100000

void generate_str(char *buf, int size) {
    size = rand() % size;
    if (size < 2)
        size = 2;
    for (int i = 0; i < size - 1; i++) {
        char c;
        while ('.' == (c = '0' + rand() % ('~' - '0')));
        *buf++ = c;
    }
    *buf = 0;
}

void generate(char *path, char *expected, int chunk, int components) {
    static char buf[MAX_SIZE];
    size_t buf_size;

    int have_initial_slash = 0;
    int last_type = 0;

    if (rand() % 100 < 25) {
        generate_str(buf, chunk);
        path = stpcpy(path, buf);
        expected = stpcpy(expected, "/");
        expected = stpcpy(expected, buf);
        have_initial_slash = 1;
        last_type = 1;
    } else if (rand() % 100 < 33) {
        path = stpcpy(path, "..");
        last_type = 2;
    } else if (rand() % 100 < 50) {
        path = stpcpy(path, ".");
        last_type = 3;
    }

    components = rand() % components;
    if (components < 1)
        components = 1;

    for (int i = 0; i < components; i++) {
        if (rand() % 100 < 25) {
            generate_str(buf, chunk);
            path = stpcpy(path, "/");
            path = stpcpy(path, buf);
            expected = stpcpy(expected, "/");
            expected = stpcpy(expected, buf);
            have_initial_slash = 1;
            last_type = 1;
        } else if (rand() % 100 < 33) {
            generate_str(buf, chunk);
            path = stpcpy(path, "/");
            path = stpcpy(path, buf);
            path = stpcpy(path, "/..");
            last_type = 2;
        } else if (rand() % 100 < 50) {
            path = stpcpy(path, "/.");
            last_type = 3;
        } else {
            path = stpcpy(path, "/");
            last_type = 4;
        }
    }

    if (!have_initial_slash || last_type == 4) {
        expected = stpcpy(expected, "/");
    }
}

int main(int argc, char **argv) {
    static char path[MAX_SIZE], expected[MAX_SIZE];

    if (argc != 5) {
        puts("fuzz <#seed> <#tests> <#components> <#component-size>");
        exit(1);
    }

    int seed = atoi(argv[1]);
    int tests = atoi(argv[2]);
    int components = atoi(argv[3]);
    int component_size = atoi(argv[4]);

    srand(seed);

    for (int i = 0; i < tests; i++) {
        generate(path, expected, component_size, components);
        fprintf(stdout, "%s\n", path);
        fprintf(stderr, "%s\n", expected);
    }

    return 0;
}
